# libthread

A collection of threadlib and it's dependencies intended for use as a single `git` `subtree`.

<tt>mosflow</tt>
<a href="https://gitlab.com/openscad1/mosflow">
<img src="https://gitlab.com/openscad1/mosflow/-/raw/main/logo/logo.png" alt="mosflow logo" height="200"/> 
</a>

<tt>threadlib</tt>
<a href="https://github.com/adrianschlatter/threadlib">
<img src="https://raw.githubusercontent.com/adrianschlatter/threadlib/bb860b61a2007d998669bc399ce2855cc2fefb64/docs/imgs/logo.png" alt="threadlib logo" />
</a>


## home, sweet home

    git@gitlab.com:openscad1/libthread.git
    
## adopt me

    git subtree add  --message 'mtools git subtree add locator - libthread\nrepo=git@gitlab.com:openscad1/libthread.git\nprefix=libthread\nbranch=main' --prefix libthread git@gitlab.com:openscad1/libthread.git main --squash


## inherit updates from the mother ship

    git subtree pull --prefix libthread git@gitlab.com:openscad1/libthread.git main --squash

## push our updates to the mother ship

    git subtree push --prefix libthread git@gitlab.com:openscad1/libthread.git main --squash


### fix subtree commit log
git subtree add  --message 'mtools git subtree add locator - scad-utils\nrepo=git@github.com:openscad/scad-utils.git\nprefix=scad-utils\nbranch=master' --prefix scad-utils git@github.com:openscad/scad-utils.git master --squash
git subtree add  --message 'mtools git subtree add locator - list-comprehension-demos\nrepo=git@github.com:openscad/list-comprehension-demos.git\nprefix=list-comprehension-demos\nbranch=master' --prefix list-comprehension-demos git@github.com:openscad/list-comprehension-demos.git master --squash
git subtree add  --message 'mtools git subtree add locator - IoP-satellite\nrepo=git@github.com:MisterHW/IoP-satellite.git\nprefix=IoP-satellite\nbranch=master' --prefix IoP-satellite git@github.com:MisterHW/IoP-satellite.git master --squash
git subtree add  --message 'mtools git subtree add locator - threadlib\nrepo=git@github.com:adrianschlatter/threadlib.git\nprefix=threadlib\nbranch=master' --prefix threadlib git@github.com:adrianschlatter/threadlib.git master --squash
git subtree add  --message 'mtools git subtree add locator - libmake\nrepo=git@gitlab.com:m5658/libmake.git\nprefix=libmake\nbranch=main' --prefix libmake  git@gitlab.com:m5658/libmake.git main --squash
